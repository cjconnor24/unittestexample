# Unit Testing Example With jUnit

This is a simple unit testing example using jUNIT as part of a Web Platform Development Module at Glasgow Caledonian University.

## Original Author / Credits

This repo has been copied from KHarttman at GCU and the original repo can be found [here on her account](https://bitbucket.org/katrinhartmann/wpd2-2017-b).
